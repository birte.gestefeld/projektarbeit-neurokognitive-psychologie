# fixation stability & accuracy in the antisaccade task
# parameters are determined for correct antisaccades and false prosaccades separately

rm(list = ls())

library(here)
library(data.table)
library(tidyverse)
library(ggplot2)
library(fields)
library(NISTunits)
library(raster)
library(foreach)
source(paste(here(), "/Data_analysis/pix2deg.R", sep = ""))
RMS = function(sig) {
  sqrt(mean((sig) ^ 2))
}
# working directory: muss für den jeweiligen Rechner stimmen
setwd(paste(here(), "/Data/Antisaccade/Session1/ReactionTimes", sep = ""))

files <- list.files(getwd(), pattern = "*.Rdata")
# used later to compute BCEA
k <- 1.14

# for each participant
for (f in 1:length(files)) {
  # load the coranti and falsepro saccades
  load(files[f])
  filename_tmp <- strsplit(files[f], split = "")
  filename <-
    paste(filename_tmp[[1]][1],
          filename_tmp[[1]][2],
          filename_tmp[[1]][3],
          filename_tmp[[1]][4],
          sep = "")
  blocks <- unique(coranti_sac$block)
  
  # extract the correct trials per block
  rms_cor <- vector(mode = "numeric")
  BCEA_cor <- vector(mode = "numeric")
  fix_dist_cor <- vector(mode = "numeric")
  dr_coranti <- data.table()
  
  # extract the incorrect trials per block
  rms_fal <- vector(mode = "numeric")
  BCEA_fal <- vector(mode = "numeric")
  fix_dist_fal <- vector(mode = "numeric")
  dr_falsepro <- data.table()
  
  for (b in 1:length(blocks)) {
    cor_trials <- coranti_sac$trial[coranti_sac$block == blocks[b]]
    fal_trials <-
      falsepro_sac$trial[falsepro_sac$block == blocks[b]]
    # load data of the fixation period
    load(
      paste(
        here(),
        "/Data/Antisaccade/Session1/",
        filename,
        "B",
        blocks[b],
        "S1 RawDataFixationPeriod.Rdata"
        ,
        sep = ""
      )
    )
    
    #extract the raw data of the correct antisaccade trials
    dr_coranti_tmp <- data.table()
    for (ct in cor_trials) {
      dr_coranti_tmp <- rbind(dr_coranti_tmp, dr[dr$trial == ct])
    }
    
    #extract the raw data of the false prosaccade trials
    dr_falsepro_tmp <- data.table()
    for (ct2 in fal_trials) {
      dr_falsepro_tmp <- rbind(dr_falsepro_tmp, dr[dr$trial == ct2])
    }
    
    #remove first 250 ms (time needed to return to fixation cross)
    dr_coranti_tmp$deg_rx[dr_coranti_tmp$ref_time <= 250] <- NA
    dr_coranti_tmp$deg_ry[dr_coranti_tmp$ref_time <= 250] <- NA
    
    #find blinks in the data
    # missing data in drpupil
    idx_missing1 <- which(dr_coranti_tmp$rpupil == 0)
    idx_end1 <- c(which(diff(idx_missing1) > 1), length(idx_missing1))
    missing_end1 <- idx_missing1[idx_end1]
    idx_start1 <- c(1, idx_end1 + 1)
    missing_start1 <- idx_missing1[idx_start1]
    
    #idx_remove_before<-vector(mode = "list", length = length(missing_start))
    if (length(missing_end1) > 0) {
      for (i in 1:length(missing_end1)) {
        rp1_start <- missing_start1[i] - 200
        if (rp1_start<1){
          rp1_start <-1
        }
        rp1_end <- missing_start1[i]
        if (!is.na(rp1_start) & !is.na(rp1_end)) {
          dr_coranti_tmp$deg_rx[rp1_start:rp1_end] <- NA
          dr_coranti_tmp$deg_ry[rp1_start:rp1_end] <- NA
          
        }
        # idx_remove_before[[i]]<-rp_start:rp_end
        rp2_start <- missing_end1[i]
        rp2_end <- missing_end1[i] + 200
        if (!is.na(rp2_start) & !is.na(rp2_end)) {
          dr_coranti_tmp$deg_rx[rp2_start:rp2_end] <- NA
          dr_coranti_tmp$deg_ry[rp2_start:rp2_end] <- NA
        }
        
      }
    }
    # plot one figure per trial
    plot_fix <- ggplot(dr_coranti_tmp) +
      geom_point(aes(x = deg_rx, y = deg_ry),
                 color = "blue",
                 size = 3) +
      geom_point(aes(x = 0, y = 0), color = "red", size = 3) +
      facet_wrap(~ trial, nrow = 6) +
      xlim(-18, 18) +
      ylim(-10, 10) +
      theme(text = element_text(size = 16))
    print(plot_fix)
    ggsave(
      plot_fix,
      filename = paste(
        here(),
        "/Figures/FixationStability/Antisaccade/Session1/CAblinksRemoved",
        filename,
        ".tiff",
        sep = ""
      ),
      height = 10 ,
      width = 18
    )
    
    
    
    # false prosaccades
    dr_falsepro_tmp$deg_rx[dr_falsepro_tmp$ref_time <= 250] <- NA
    dr_falsepro_tmp$deg_ry[dr_falsepro_tmp$ref_time <= 250] <- NA
    
    idx_missing2 <- which(dr_falsepro_tmp$rpupil == 0)
    idx_end2 <- c(which(diff(idx_missing2) > 1), length(idx_missing2))
    missing_end2 <- idx_missing2[idx_end2]
    idx_start2 <- c(1, idx_end2 + 1)
    missing_start2 <- idx_missing2[idx_start2]
    
    #idx_remove_before<-vector(mode = "list", length = length(missing_start))
    if (length(missing_end2) > 0) {
      for (i in 1:length(missing_end2)) {
        rp1_start2 <- missing_start2[i] - 200
        if (rp1_start2<1){
          rp1_start2 <-1
        }
        rp1_end2 <- missing_start2[i]
        if (!is.na(rp1_start2) & !is.na(rp1_end2)) {
          dr_falsepro_tmp$deg_rx[rp1_start2:rp1_end2] <- NA
          dr_falsepro_tmp$deg_ry[rp1_start2:rp1_end2] <- NA
          
        }
        # idx_remove_before[[i]]<-rp_start:rp_end
        rp2_start2 <- missing_end2[i]
        rp2_end2 <- missing_end2[i] + 200
        if (!is.na(rp2_start2) & !is.na(rp2_end2)) {
          dr_falsepro_tmp$deg_rx[rp2_start2:rp2_end2] <- NA
          dr_falsepro_tmp$deg_ry[rp2_start2:rp2_end2] <- NA
        }
        
      }
    }
    
    # plot one figure per trial
    plot_fix <- ggplot(dr_falsepro_tmp) +
      geom_point(aes(x = deg_rx, y = deg_ry),
                 color = "blue",
                 size = 3) +
      geom_point(aes(x = 0, y = 0), color = "red", size = 3) +
      facet_wrap(~ trial, nrow = 6) +
      xlim(-18, 18) +
      ylim(-10, 10) +
      theme(text = element_text(size = 16))
    print(plot_fix)
    ggsave(
      plot_fix,
      filename = paste(
        here(),
        "/Figures/FixationStability/Antisaccade/Session1/FPblinksRemoved",
        filename,
        ".tiff",
        sep = ""
      ),
      height = 10 ,
      width = 18
    )
    
    
    #compute distance between adjacent samples (blinks removed)
    sig_tmp <-
      setDT(dr_coranti_tmp)[, sqrt(Reduce(`+`, frollapply(.SD, 2L, function(v)
        diff(v) ^ 2))), trial, .SDcols = deg_rx:deg_ry]
    sig_tmp <- na.omit(sig_tmp)
    
    if (nrow(dr_falsepro_tmp) > 0) {
      sig_tmp_fal <-
        setDT(dr_falsepro_tmp)[, sqrt(Reduce(`+`, frollapply(.SD, 2L, function(v)
          diff(v) ^ 2))), trial, .SDcols = deg_rx:deg_ry]
      sig_tmp_fal <- na.omit(sig_tmp_fal)
    }
    
    #root mean square
    rms_cor_tmp <- vector(mode = "numeric")
    i <- 1
    for (t in unique(sig_tmp$trial)) {
      idx = which(sig_tmp$trial == t)
      #compute rms
      rms_cor_tmp[i] <- RMS(sig_tmp[idx]$V1)
      i <- i + 1
    }
    
    # compute BCEA
    # first mean and sd over both dimensions
    BCEA_cor_tmp <- vector(mode = "numeric")
    fix_dist_tmp <- vector(mode = "numeric")
    i <- 1
    for (t in unique(dr_coranti_tmp$trial)) {
      idx = which(sig_tmp$trial == t)
      sdx <- sd(na.omit(dr_coranti_tmp[idx]$deg_rx))
      sdy <- sd(na.omit(dr_coranti_tmp[idx]$deg_ry))
      mux <- mean(na.omit(dr_coranti_tmp[idx]$deg_rx))
      muy <- mean(na.omit(dr_coranti_tmp[idx]$deg_ry))
      mu <- c(mux, muy)
      pcor <-
        cor(na.omit(dr_coranti_tmp[idx]$deg_rx),
            na.omit(dr_coranti_tmp[idx]$deg_ry),
            method = "pearson")
      BCEA_cor_tmp[i] <- 2 * k * sdy * sdx * (1 - pcor) ^ 0.5
      
      # compute the Euclidean distance from fixation cross
      fix_dist_tmp[i] <-
        mean(pointDistance(
          cbind(dr_coranti_tmp[idx]$deg_rx, dr_coranti_tmp[idx]$deg_ry),
          c(0, 0),
          lonlat = FALSE
        ), na.rm = TRUE)
      i <- i + 1
    }
    
    #false prosaccade
    i <- 1
    rms_fal_tmp <- vector(mode = "numeric")
    if (nrow(dr_falsepro_tmp) > 0) {
      for (t in unique(sig_tmp_fal$trial)) {
        idx = which(sig_tmp_fal$trial == t)
        #compute rms
        rms_fal_tmp[i] <- RMS(sig_tmp_fal[idx]$V1)
        i <- i + 1
      }
    }
    
    
    i <- 1
    BCEA_fal_tmp <- vector(mode = "numeric")
    fix_dist_fal_tmp <- vector(mode = "numeric")
    if (nrow(dr_falsepro_tmp) > 0) {
      for (t in unique(dr_falsepro_tmp$trial)) {
        idx = which(dr_falsepro_tmp$trial == t)
        # compute BCEA
        # first mean and sd over both dimensions
        sdx <- sd(na.omit(dr_falsepro_tmp[idx]$deg_rx))
        sdy <- sd(na.omit(dr_falsepro_tmp[idx]$deg_ry))
        mux <- mean(na.omit(dr_falsepro_tmp[idx]$deg_rx))
        muy <- mean(na.omit(dr_falsepro_tmp[idx]$deg_ry))
        mu <- c(mux, muy)
        pcor <-
          cor(
            na.omit(dr_falsepro_tmp[idx]$deg_rx),
            na.omit(dr_falsepro_tmp[idx]$deg_ry),
            method = "pearson"
          )
        BCEA_fal_tmp[i] <- 2 * k * sdy * sdx * (1 - pcor) ^ 0.5
        
        # compute the Euclidean distance from fixation cross
        fix_dist_fal_tmp[i] <-
          mean(pointDistance(
            cbind(dr_falsepro_tmp[idx]$deg_rx, dr_falsepro_tmp[idx]$deg_ry),
            c(0, 0),
            lonlat = FALSE
          ), na.rm = TRUE)
        i <- i + 1
      }
    }
    
    
    
    #collect data across blocks
    rms_cor <- rbind(rms_cor, rms_cor_tmp)
    BCEA_cor <- rbind(BCEA_cor, BCEA_cor_tmp)
    fix_dist_cor <- rbind(fix_dist_cor, fix_dist_tmp)
    if (nrow(dr_falsepro_tmp) > 0) {
      rms_fal <- rbind(rms_fal, rms_fal_tmp)
      BCEA_fal <- rbind(BCEA_fal, BCEA_fal_tmp)
      fix_dist_fal <- rbind(fix_dist_fal, fix_dist_fal_tmp)
    }
  }
  
  save(
    rms_cor,
    BCEA_cor,
    fix_dist_cor,
    rms_fal,
    BCEA_fal,
    fix_dist_fal,
    file = paste(
      here(),
      "/Data/Antisaccade/Session1/FixationStability/",
      filename,
      ".Rdata",
      sep = ""
    )
  )
  
}



