rm(list = ls())
library(psych)
library("scatterplot3d")
library("irr")
library(BlandAltmanLeh)
library(here)
# skript kann angpasst werden 
# Daten der ersten Sitzung 
files_ses1 <-
  list.files(
   paste(here(),"/Data/Antisaccade/Session1/ReactionTimes/MeanRT/",sep="")
  )
  mean_ses1<-vector(mode='numeric')
  for (f in 1:length(files_ses1)) {
    load(paste(here(),"/Data/Antisaccade/Session1/ReactionTimes/MeanRT/",files_ses1[f],sep=""))
    mean_ses1 <- c(mean_ses1, mean_log)
  }
  
  # Daten der 2. Sitzung 
files_ses2 <-
  list.files(
    paste(here(),"/Data/Antisaccade/Session2/ReactionTimes/MeanRT/",sep="")
  )
mean_ses2<-vector(mode='numeric')
for (f in 1:length(files_ses2)) {
  load(paste(here(),"/Data/Antisaccade/Session2/ReactionTimes/MeanRT/",files_ses2[f],sep=""))
      mean_ses2 <- c(mean_ses2, mean_log)
    }
    

# Daten der 3. Sitzung 
files_ses3 <-
  list.files(
    paste(here(),"/Data/Antisaccade/Session3/ReactionTimes/MeanRT/",sep="")
  )
mean_ses3<-vector(mode='numeric')
for (f in 1:length(files_ses2)) {
  load(paste(here(),"/Data/Antisaccade/Session3/ReactionTimes/MeanRT/",files_ses3[f],sep=""))
  mean_ses3 <- c(mean_ses3, mean_log)
}

data_rt<-data.frame(
  ses1=mean_ses1,
  ses2=mean_ses2,
  ses3=mean_ses3
)

ICC_statistics<-icc(data_rt, model = "twoway",
    type = "agreement", unit = "single")


#Bland Altmann plots
bland.altman.plot(data_rt[,1], data_rt[,3], graph.sys = "ggplot2")
ggsave(
  BA_mediandist,
  filename = paste(
    here(), "/Figures/BlandAltmann/Anitsaccades/RTSession1&3.tiff", # Namen anpassen 
    sep = ""
  )
)

tiff(file=paste(here(),"/Figures/Correlation/Antisaccades/CorrelationRT.tiff", sep=""),
     width=6, height=4, units="in", res=100)
scatterplot3d(data_rt, pch=16)
dev.off()
